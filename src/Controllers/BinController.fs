﻿namespace which_bin.Controllers

open System
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open System.Net
open System.IO
open System.Globalization

[<ApiController>]
[<Route("[controller]")>]
type BinController (logger : ILogger<BinController>) =
    inherit ControllerBase()

    [<HttpGet>]
    member __.Get() : string =
        let req = WebRequest.Create(Uri("https://maps.staffsmoorlands.gov.uk/map2014/Cluster.svc/getpage?script=\Cluster\HP\HPBins.AuroraScript$&taskId=HPbins&format=js&updateOnly=true&query=x%3D403852.25%3By%3D393966.91%3Bid%3D57268")) 
        use resp = req.GetResponse() 
        use stream = resp.GetResponseStream() 
        use reader = new StreamReader(stream)
        let text = reader.ReadToEnd()
        let textArray = text.Split(' ')
        let blackBinIndex = Array.tryFindIndex(fun x -> x = @"class=\u0027wasterBLACK\u0027\u003eBLACK\u003c/span\u003e") textArray
        let brownBinIndex = Array.tryFindIndex(fun x -> x = @"class=\u0027wasterRED\u0027\u003ered\u003c/span\u003e") textArray
        let blackBinDay = textArray.[blackBinIndex.Value + 3]
        let blackBinDateString = textArray.[blackBinIndex.Value + 4]
        let brownBinDay = textArray.[brownBinIndex.Value + 3]
        let brownBinDateString = textArray.[brownBinIndex.Value + 4]
        let ukCulture = CultureInfo.CreateSpecificCulture("en-GB")
        let blackBinDate = DateTime.Parse(blackBinDateString, ukCulture)
        let brownBinDate = DateTime.Parse(brownBinDateString, ukCulture)
        match (blackBinDay, blackBinDate) with
        | ("Tuesday", blackBinDate) ->
            if (blackBinDate < brownBinDate)
            then "Black bin"
            else "Brown bin"
        | (_, blackBinDate) ->
            if(blackBinDate < brownBinDate)
            then "Black bin on " + blackBinDay
            else "Brown bin on " + brownBinDay
